%global commit  b90dc212
%global nm_version 1.2
%global nma_version 1.0
%global glib_required_version 2.63.1
%global gtk_required_version 3.11.3
%global geoclue_version 2.3.1
 
Name:           gnome-initial-setup
Version:        3.38.1+b90dc212
Release:        2%{?dist}
Summary:        Bootstrapping your OS
 
License:        GPLv2+
URL:            https://wiki.gnome.org/Design/OS/InitialSetup
Source0:        hhttps://source.puri.sm/pureos/core/gnome-initial-setup/-/archive/%{commit}/gnome-initial-setup-%{commit}.tar.gz
Source1:        vendor.conf
 
BuildRequires:  meson
BuildRequires:  gcc
BuildRequires:  krb5-devel
BuildRequires:  desktop-file-utils
BuildRequires:  libpwquality-devel
BuildRequires:  pkgconfig(libnm) >= %{nm_version}
BuildRequires:  pkgconfig(libnma) >= %{nma_version}
BuildRequires:  pkgconfig(accountsservice)
BuildRequires:  pkgconfig(gnome-desktop-3.0)
BuildRequires:  pkgconfig(gstreamer-1.0)
BuildRequires:  pkgconfig(cheese)
BuildRequires:  pkgconfig(cheese-gtk) >= 3.3.5
BuildRequires:  pkgconfig(fontconfig)
BuildRequires:  pkgconfig(geocode-glib-1.0)
BuildRequires:  pkgconfig(gweather-3.0)
BuildRequires:  pkgconfig(goa-1.0)
BuildRequires:  pkgconfig(goa-backend-1.0)
BuildRequires:  pkgconfig(gtk+-3.0) >= %{gtk_required_version}
BuildRequires:  pkgconfig(glib-2.0) >= %{glib_required_version}
BuildRequires:  pkgconfig(gio-2.0) >= %{glib_required_version}
BuildRequires:  pkgconfig(gio-unix-2.0) >= %{glib_required_version}
BuildRequires:  pkgconfig(gdm)
BuildRequires:  pkgconfig(iso-codes)
BuildRequires:  pkgconfig(libgeoclue-2.0) >= %{geoclue_version}
BuildRequires:  pkgconfig(packagekit-glib2)
BuildRequires:  pkgconfig(webkit2gtk-4.0)
BuildRequires:  krb5-devel
BuildRequires:  ibus-devel
BuildRequires:  rest-devel
BuildRequires:  polkit-devel
BuildRequires:  libsecret-devel
 
# gnome-initial-setup is being run by gdm
Requires: gdm
Requires: geoclue2-libs%{?_isa} >= %{geoclue_version}
Requires: glib2%{?_isa} >= %{glib_required_version}
# gnome-initial-setup runs gnome-tour at the end of an initial setup run
Requires: gnome-tour
# we install a rules file
Requires: polkit-js-engine
Requires: /usr/bin/gkbd-keyboard-display
 
Requires(pre): shadow-utils
 
Provides: user(%name)
%description
GNOME Initial Setup is an alternative to firstboot, providing
a good setup experience to welcome you to your system, and walks
you through configuring it. It is integrated with gdm.
 
%prep
%autosetup -p1
 
%build
%meson -Dparental_controls=disabled -Dvendor-conf-file=%{_datadir}/gnome-initial-setup/vendor.conf
%meson_build
%install
%meson_install
# Desktop file does not (and probably will not) ever validate, as it uses
# an absolute path /tmp/-style trigger to determine whether to autostart.
# desktop-file-validate %%{buildroot}/%%{_sysconfdir}/xdg/autostart/gnome-welcome-tour.desktop
desktop-file-validate %{buildroot}%{_sysconfdir}/xdg/autostart/gnome-initial-setup-copy-worker.desktop
desktop-file-validate %{buildroot}%{_datadir}/gdm/greeter/applications/gnome-initial-setup.desktop
 
mkdir -p %{buildroot}%{_datadir}/gnome-initial-setup
cp %{SOURCE1} %{buildroot}%{_datadir}/gnome-initial-setup/
 
%find_lang %{name}
 
%pre
useradd -rM -d /run/gnome-initial-setup/ -s /sbin/nologin %{name} &>/dev/null || :
 
%files -f %{name}.lang
%license COPYING
%doc README.md
%{_libexecdir}/gnome-initial-setup
%{_libexecdir}/gnome-initial-setup-copy-worker
%{_libexecdir}/gnome-welcome-tour
%{_sysconfdir}/xdg/autostart/gnome-welcome-tour.desktop
%{_sysconfdir}/xdg/autostart/gnome-initial-setup-copy-worker.desktop
%{_sysconfdir}/xdg/autostart/gnome-initial-setup-first-login.desktop
 
%{_datadir}/gdm/greeter/applications/gnome-initial-setup.desktop
%{_datadir}/gnome-session/sessions/gnome-initial-setup.session
%{_datadir}/gnome-shell/modes/initial-setup.json
%{_datadir}/polkit-1/rules.d/20-gnome-initial-setup.rules
%{_userunitdir}/*
 
%dir %{_datadir}/gnome-initial-setup
%{_datadir}/gnome-initial-setup/vendor.conf
